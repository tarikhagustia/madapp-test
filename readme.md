# MADAPP TEST DOCUMENTATION #

### End Point ###
| End Point     | Type           | Comment  | DATA  |
| ------------- |:-------------:| -----:| -----:|
| ```/api/user```   | ```POST```    | Buat User Baru | `username, email, password`|
| ```/api/user```   | ```GET```     | Ambil Semua Data User | -
| ```/api/user/:id```   | ```GET```      |    Ambil Detail User | `id`
| ```/api/user/delete/:id```   | ```POST```      |    Hapus User | - |
| ```/api/user/update/:id```   | ```PUT```      |    Change User Detail | `username, email, password, status` |
