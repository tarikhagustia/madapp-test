<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateUser()
    {
      $this->json('POST', '/api/user', [
        'username' => 'Fulan Test',
        'password' => 'jimblo',
        'email' => 'jomblo@gmail.com'
      ])->seeJson([
          'status' => true,
      ]);
    }

    public function testGetUser()
    {
      $this->json('GET', '/api/user')->seeJsonStructure([
          'current_page'
      ]);
    }
}
