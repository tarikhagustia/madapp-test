<?php

use Illuminate\Http\Request;

use App\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Buat User Baru
Route::post('/user', function(Request $request){
  // dd($request->all());
  try {
    $sql = new User;
    $sql->username = $request->username;
    $sql->password = $request->password;
    $sql->email = $request->email;
    $sql->status = 'active';
    $sql->password = bcrypt($request->password);
    $sql->save();
    return response()->json(['status' => true, 'message' => 'Success create user']);
  } catch (\Exception $e) {
    return response()->json(['error' => 'failed create user ' . $e->getMessage()], 500);
  }

});


// Ambil User berdasarkan page, pakai ?page=no_page
Route::get('/user', function (Request $request) {
  return User::paginate(2);
});


// View User Detail

Route::get('/user/{id}', function($id){

  $user = User::find($id);

  if(!$user)
    return response()->json(["error" => "User Not Found!"], 404);

  return $user;

});


// Note: pakai www form data
Route::put('/user/update/{id}', function($id, Request $request){

  $user = User::where('id', $id)->update($request->all());

  if($user):
    return response()->json(['status' => true, 'message' => "success update data"]);
  else:
    return response()->json(['error' => "failed update data"], 500);
  endif;


});


// Delete user
Route::post('/user/delete/{id}', function($id){
  $find = User::find($id);

  if(!$find)
    return response()->json(['error' => 'User Not Found !'], 404);

  $find->status = "archived";
  $find->save();

  return response()->json(['status' => true, 'message' => 'Success Update Data'], 404);
});
