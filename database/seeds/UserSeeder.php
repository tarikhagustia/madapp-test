<?php

use Illuminate\Database\Seeder;

use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $limit = 10;

        for ($i=0; $i < $limit; $i++) {
          User::create([
            'username' => "Tarikh Ganteng " . $i,
            'password' => bcrypt(1234),
            'email' => 'agustia.tarikh150'.$i.'@gmail.com',
            'status' => 'active'
          ]);
        }
    }
}
